﻿using System;
using bpac;

using System.Windows.Forms;

public class TDispLabel
{
    public string name { get; set; }
    public string nickname { get; set; }
    public string barcode { get; set; }
    public string additional { get; set; }
    public string id { get; set;  }

    bpac.DocumentClass doc;

    public TDispLabel(string name, string nickname, string barcode, string additional)
	{
        this.name = name;
        this.nickname = nickname;
        this.barcode = barcode;
        this.additional = additional;
        this.id = id;

        this.doc = new DocumentClass();

    }
    public TDispLabel()
    {

    }

    public void print(string templatePath, int count)
    {
        string timestamp = System.DateTime.Now.ToString("dd.MM.yyyy HH:mm");
          if (doc.Open(templatePath) != false)
        {
            doc.GetObject("title").Text = name;
            doc.GetObject("barcode").Text = barcode;
            doc.GetObject("nickname").Text = nickname;
            doc.GetObject("timestamp").Text = timestamp;

            bpac.PrintOptionConstants printOptions = PrintOptionConstants.bpoAutoCut | PrintOptionConstants.bpoQuality;
            doc.StartPrint("", printOptions);
            doc.PrintOut(count, printOptions);
            doc.EndPrint();
            doc.Close();
        }
        else
        {
            MessageBox.Show("Open() Error: " + doc.ErrorCode + "\nMedia ID" + doc.Printer.GetMediaId().ToString());
        }
    }
}
