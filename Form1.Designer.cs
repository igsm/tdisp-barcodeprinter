﻿namespace BarcodePrint
{
    partial class BarcodeForm
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BarcodeForm));
            this.btnPrint = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.cntCopies = new System.Windows.Forms.NumericUpDown();
            this.timestamp = new System.Windows.Forms.DateTimePicker();
            this.list_available = new System.Windows.Forms.ListBox();
            this.lbl_list_lastUpdate = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbl_state = new System.Windows.Forms.Label();
            this.btn_updateList = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtNickname = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lbl_templateState = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbl_mediaType = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btn_saveSettings = new System.Windows.Forms.Button();
            this.txt_baseURI = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btn_deleteLabel = new System.Windows.Forms.Button();
            this.timer_server = new System.Windows.Forms.Timer(this.components);
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.cntCopies)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(6, 44);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(594, 25);
            this.btnPrint.TabIndex = 2;
            this.btnPrint.Text = "Print label";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Barcode";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Count";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(312, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Timestamp";
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(68, 18);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(226, 20);
            this.txtTitle.TabIndex = 8;
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(68, 70);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(226, 20);
            this.txtBarcode.TabIndex = 9;
            // 
            // cntCopies
            // 
            this.cntCopies.Location = new System.Drawing.Point(63, 18);
            this.cntCopies.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.cntCopies.Name = "cntCopies";
            this.cntCopies.Size = new System.Drawing.Size(231, 20);
            this.cntCopies.TabIndex = 9;
            this.cntCopies.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // timestamp
            // 
            this.timestamp.CustomFormat = "dd.MM.yyyy hh:mm";
            this.timestamp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timestamp.Location = new System.Drawing.Point(374, 18);
            this.timestamp.MaxDate = new System.DateTime(2016, 11, 24, 21, 40, 25, 198);
            this.timestamp.Name = "timestamp";
            this.timestamp.Size = new System.Drawing.Size(226, 20);
            this.timestamp.TabIndex = 9;
            this.timestamp.Value = new System.DateTime(2016, 11, 24, 21, 40, 25, 198);
            // 
            // list_available
            // 
            this.list_available.FormattingEnabled = true;
            this.list_available.Location = new System.Drawing.Point(6, 19);
            this.list_available.Name = "list_available";
            this.list_available.Size = new System.Drawing.Size(288, 121);
            this.list_available.TabIndex = 10;
            this.list_available.SelectedIndexChanged += new System.EventHandler(this.list_available_SelectedIndexChanged);
            // 
            // lbl_list_lastUpdate
            // 
            this.lbl_list_lastUpdate.AutoSize = true;
            this.lbl_list_lastUpdate.Location = new System.Drawing.Point(6, 143);
            this.lbl_list_lastUpdate.Name = "lbl_list_lastUpdate";
            this.lbl_list_lastUpdate.Size = new System.Drawing.Size(62, 13);
            this.lbl_list_lastUpdate.TabIndex = 11;
            this.lbl_list_lastUpdate.Text = "last update:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbl_state);
            this.groupBox1.Controls.Add(this.btn_updateList);
            this.groupBox1.Controls.Add(this.list_available);
            this.groupBox1.Controls.Add(this.lbl_list_lastUpdate);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(300, 192);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pending Labels";
            // 
            // lbl_state
            // 
            this.lbl_state.AutoSize = true;
            this.lbl_state.Location = new System.Drawing.Point(66, 143);
            this.lbl_state.Name = "lbl_state";
            this.lbl_state.Size = new System.Drawing.Size(72, 13);
            this.lbl_state.TabIndex = 13;
            this.lbl_state.Text = "no update yet";
            // 
            // btn_updateList
            // 
            this.btn_updateList.Location = new System.Drawing.Point(6, 159);
            this.btn_updateList.Name = "btn_updateList";
            this.btn_updateList.Size = new System.Drawing.Size(285, 23);
            this.btn_updateList.TabIndex = 12;
            this.btn_updateList.Text = "update list";
            this.btn_updateList.UseVisualStyleBackColor = true;
            this.btn_updateList.Click += new System.EventHandler(this.btn_updateList_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_deleteLabel);
            this.groupBox2.Controls.Add(this.txtNickname);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtTitle);
            this.groupBox2.Controls.Add(this.txtBarcode);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(312, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(300, 130);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Edit Label";
            // 
            // txtNickname
            // 
            this.txtNickname.Location = new System.Drawing.Point(68, 44);
            this.txtNickname.Name = "txtNickname";
            this.txtNickname.Size = new System.Drawing.Size(226, 20);
            this.txtNickname.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Nickname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(585, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "This helper program recieves pending labels from a TDisp server instance and allo" +
    "ws printing them using a P-Touch Printer.";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnPrint);
            this.groupBox3.Controls.Add(this.cntCopies);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.timestamp);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(6, 204);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(606, 77);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Print";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lbl_templateState);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.lbl_mediaType);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Location = new System.Drawing.Point(312, 142);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(300, 56);
            this.groupBox4.TabIndex = 16;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Print Media";
            // 
            // lbl_templateState
            // 
            this.lbl_templateState.AutoSize = true;
            this.lbl_templateState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_templateState.Location = new System.Drawing.Point(248, 23);
            this.lbl_templateState.Name = "lbl_templateState";
            this.lbl_templateState.Size = new System.Drawing.Size(14, 13);
            this.lbl_templateState.TabIndex = 3;
            this.lbl_templateState.Text = "?";
            this.lbl_templateState.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(162, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Template state:";
            // 
            // lbl_mediaType
            // 
            this.lbl_mediaType.AutoSize = true;
            this.lbl_mediaType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_mediaType.Location = new System.Drawing.Point(82, 23);
            this.lbl_mediaType.Name = "lbl_mediaType";
            this.lbl_mediaType.Size = new System.Drawing.Size(42, 13);
            this.lbl_mediaType.TabIndex = 1;
            this.lbl_mediaType.Text = "wait...";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Current type:";
            // 
            // timer
            // 
            this.timer.Interval = 5000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 36);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(629, 311);
            this.tabControl1.TabIndex = 17;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(621, 285);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Print";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.checkBox1);
            this.tabPage2.Controls.Add(this.btn_saveSettings);
            this.tabPage2.Controls.Add(this.txt_baseURI);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(621, 285);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Settings";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btn_saveSettings
            // 
            this.btn_saveSettings.Location = new System.Drawing.Point(269, 234);
            this.btn_saveSettings.Name = "btn_saveSettings";
            this.btn_saveSettings.Size = new System.Drawing.Size(75, 23);
            this.btn_saveSettings.TabIndex = 2;
            this.btn_saveSettings.Text = "save";
            this.btn_saveSettings.UseVisualStyleBackColor = true;
            this.btn_saveSettings.Click += new System.EventHandler(this.btn_saveSettings_Click);
            // 
            // txt_baseURI
            // 
            this.txt_baseURI.Location = new System.Drawing.Point(30, 47);
            this.txt_baseURI.Name = "txt_baseURI";
            this.txt_baseURI.Size = new System.Drawing.Size(538, 20);
            this.txt_baseURI.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(27, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(149, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "tDisp instance api endpoint uri";
            // 
            // btn_deleteLabel
            // 
            this.btn_deleteLabel.Location = new System.Drawing.Point(9, 96);
            this.btn_deleteLabel.Name = "btn_deleteLabel";
            this.btn_deleteLabel.Size = new System.Drawing.Size(285, 23);
            this.btn_deleteLabel.TabIndex = 12;
            this.btn_deleteLabel.Text = "remove label";
            this.btn_deleteLabel.UseVisualStyleBackColor = true;
            this.btn_deleteLabel.Click += new System.EventHandler(this.btn_deleteLabel_Click);
            // 
            // timer_server
            // 
            this.timer_server.Interval = 60000;
            this.timer_server.Tick += new System.EventHandler(this.timer_server_Tick);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(30, 88);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(209, 17);
            this.checkBox1.TabIndex = 3;
            this.checkBox1.Text = "automatically update pending labels list";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // BarcodeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 356);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BarcodeForm";
            this.Text = "TDisp Label Printer";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cntCopies)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker timestamp;
        private System.Windows.Forms.NumericUpDown cntCopies;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.ListBox list_available;
        private System.Windows.Forms.Label lbl_list_lastUpdate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_updateList;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNickname;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbl_state;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lbl_templateState;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbl_mediaType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btn_saveSettings;
        private System.Windows.Forms.TextBox txt_baseURI;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btn_deleteLabel;
        private System.Windows.Forms.Timer timer_server;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}

