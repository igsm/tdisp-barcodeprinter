/***********************************************************************
			b-PAC 3.0 Component Sample (TestPrint)

			(C)Copyright Brother Industries, Ltd. 2009
************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using bpac;
using System.IO;
using RestSharp;
using RestSharp.Deserializers;

namespace BarcodePrint
{
    public partial class BarcodeForm : Form
    {

        bpac.DocumentClass doc;
        List<TDispLabel> availableLabels = new List<TDispLabel>();
        TDispLabel activeLabel;

        public BarcodeForm()
        {
            InitializeComponent();
            timer.Start();
            timer_server.Start();
        }
        private void Form1_Load(object sender, EventArgs e)
        {

           doc = new DocumentClass();
            txt_baseURI.Text = Properties.Settings.Default.serverBaseURI;

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            TDispLabel l1 = new TDispLabel(txtTitle.Text, txtNickname.Text, txtBarcode.Text, "");
            string templatePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\templates\\" + doc.Printer.GetMediaId().ToString() + ".lbx";
            l1.print(templatePath, (int)cntCopies.Value);
            
        }

        private void updateList()
        {
            try
            {
                var client = new RestClient(new Uri(Properties.Settings.Default.serverBaseURI));
                var request = new RestRequest("labels", Method.GET);
                client.AddHandler("*", new JsonDeserializer());

                var response = client.Execute<SpringDTO>(request);

                list_available.Items.Clear();
                availableLabels.Clear();
        
                foreach (var label in response.Data._embedded.label)
                {
                    list_available.Items.Add(string.Format("{0} / {1} ({2})", label.name,
                                            label.nickname,
                                            label.barcode));
                    availableLabels.Add(label);
                }

                lbl_state.Text = System.DateTime.Now.ToString("dd.MM.yyyy HH:mm");
            } catch
            {

            }
           

        }
            
        private void timer_Tick(object sender, EventArgs e)
        {
            

            try
            {
                // Update printer media type 
                String MediaId = doc.Printer.GetMediaId().ToString();
                lbl_mediaType.Text = MediaId;
                lbl_mediaType.ForeColor = System.Drawing.Color.DarkGreen;
                if (MediaId == "0")
                {
                    lbl_mediaType.Text = "no printer / busy";
                    lbl_mediaType.ForeColor = System.Drawing.Color.DarkRed;
                }


                // check if template for media type exists
                String templatePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\templates\\" + doc.Printer.GetMediaId().ToString() + ".lbx";
                if (File.Exists(templatePath))
                {
                    lbl_templateState.Text = "ok";
                    lbl_templateState.ForeColor = System.Drawing.Color.DarkGreen;
                }
                else
                {
                    lbl_templateState.Text = "no tpl!";
                    lbl_templateState.ForeColor = System.Drawing.Color.DarkRed;
                }

            } catch
            {

            }
           
        }

        private void btn_saveSettings_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.serverBaseURI = txt_baseURI.Text;
            Properties.Settings.Default.Save();
        }

        private void btn_updateList_Click(object sender, EventArgs e)
        {
            updateList();
        }

        private void list_available_SelectedIndexChanged(object sender, EventArgs e)
        {
            activeLabel = availableLabels[list_available.SelectedIndex];

            txtTitle.Text = activeLabel.name;
            txtNickname.Text = activeLabel.nickname;
            txtBarcode.Text = activeLabel.barcode;
        }

        private void btn_deleteLabel_Click(object sender, EventArgs e)
        {
            var client = new RestClient(new Uri(Properties.Settings.Default.serverBaseURI));
            var request = new RestRequest("labels/{id}", Method.DELETE);
            request.AddUrlSegment("id", activeLabel.id); // replaces matching token in request.Resource
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            if (response.StatusCode == System.Net.HttpStatusCode.NoContent) {
                MessageBox.Show("Label gel�scht!");
                updateList();
            } else
            {
                MessageBox.Show("Fehler beim L�schen des Labels :(");
            }



        }

        private void timer_server_Tick(object sender, EventArgs e)
        {
            updateList();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                timer_server.Start();
            } else
            {
                timer_server.Stop();
            }
            Properties.Settings.Default.autoUpdate = checkBox1.Checked;
            Properties.Settings.Default.Save();

            
        }
    }
}