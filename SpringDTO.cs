﻿using System.Collections.Generic;

namespace BarcodePrint
{
    class SpringDTO
    {
        public LabelDTO _embedded { get; set; }
    }

    class LabelDTO
    {
        public List<TDispLabel> label { get; set; }
    }
}